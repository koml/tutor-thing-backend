from rest_framework.serializers import ModelSerializer
from learning_loop.courses.models import VideoQuestionChoice, QuizChoice

class VideoQuestionChoiceSerializer(ModelSerializer):
    class Meta:
        model = VideoQuestionChoice
        fields = '__all__'


class QuizChoiceSerializer(ModelSerializer):
    class Meta:
        model = QuizChoice
        fields = '__all__'
