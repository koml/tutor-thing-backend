from rest_framework.serializers import ModelSerializer

from learning_loop.courses.models import Course

from .section_serializer import SectionSerializer

class CourseSerializer(ModelSerializer):
    sections = SectionSerializer(
        many=True, 
        read_only=True
    )

    class Meta:
        model = Course
        fields = '__all__'