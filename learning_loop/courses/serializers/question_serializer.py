from rest_framework.serializers import ModelSerializer

from learning_loop.courses.models import VideoQuestionPair, QuizQuestion

from .answer_choice_serializer import (QuizChoiceSerializer,
                                       VideoQuestionChoiceSerializer)


class VideoQuestionPairSerializer(ModelSerializer):
    video_question_choices = VideoQuestionChoiceSerializer(
        many=True, 
        read_only=True
    )

    class Meta:
        model = VideoQuestionPair
        fields = '__all__'


class QuizQuestionSerializer(ModelSerializer):
    quiz_choices = QuizChoiceSerializer(
        many=True,
        read_only=True
    )

    class Meta:
        model = QuizQuestion
        fields = '__all__'
