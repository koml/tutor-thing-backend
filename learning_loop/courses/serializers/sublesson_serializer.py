from rest_framework.serializers import ModelSerializer

from learning_loop.courses.models import Sublesson

from .question_serializer import VideoQuestionPairSerializer


class SublessonSerializer(ModelSerializer):
    video_question_pairs = VideoQuestionPairSerializer(
        many=True,
        read_only=True
    )

    class Meta:
        model = Sublesson
        fields = '__all__'
