from rest_framework.serializers import ModelSerializer

from learning_loop.courses.models import Lesson

from .question_serializer import QuizQuestionSerializer
from .sublesson_serializer import SublessonSerializer


class LessonSerializer(ModelSerializer):
    sublessons = SublessonSerializer(
        many=True,
        read_only=True
    )

    quiz_questions = QuizQuestionSerializer(
        many=True,
        read_only=True
    )

    class Meta:
        model = Lesson
        fields = '__all__'
