from rest_framework.serializers import ModelSerializer

from learning_loop.courses.models import Section

from .lesson_serializer import LessonSerializer

class SectionSerializer(ModelSerializer):
    lessons = LessonSerializer(
        many=True,
        read_only=True
    )

    class Meta:
        model = Section
        fields = '__all__'
