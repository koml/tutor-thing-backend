from django.contrib import admin

from .models import Course, Section, Lesson, Sublesson, VideoQuestionPair, QuizQuestion, VideoQuestionChoice, QuizChoice

admin.site.register(Course)
admin.site.register(Section)
admin.site.register(Lesson)
admin.site.register(Sublesson)
admin.site.register(VideoQuestionPair)
admin.site.register(QuizQuestion)
admin.site.register(VideoQuestionChoice)
admin.site.register(QuizChoice)

