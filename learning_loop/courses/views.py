from rest_framework.generics import (ListCreateAPIView,
                                     RetrieveUpdateDestroyAPIView)
from rest_framework.views import APIView
from rest_framework.response import Response

from .serializers import CourseSerializer, LessonSerializer
from .models import Course, Section, Lesson


class CourseListView(ListCreateAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer


class LessonDetailView(APIView):
    def get(self, request, format=None, *args, **kwargs):
        try:
            course_title = kwargs['course_title']
            section_title = kwargs['section_title']
            lesson_title = kwargs['lesson_title']
            course = Course.objects.get(short_title=course_title)
            section = Section.objects.get(
                course=course,
                short_title=section_title
            )
            lesson = Lesson.objects.get(
                section=section, 
                short_title=lesson_title
            )
            print(lesson)
            return Response(data=LessonSerializer(lesson).data)
        except KeyError:
            return Response(data='Could not find the lesson', status=404)
