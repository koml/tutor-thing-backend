from django.db import models
from .course import Course

class Section(models.Model):
    title = models.TextField(max_length=100)

    short_title = models.TextField(max_length=20)

    code = models.IntegerField()

    course = models.ForeignKey(
        Course,
        on_delete=models.CASCADE,
        related_name='sections'
    )

    def __str__(self):
        return '{} ({})'.format(self.title, self.short_title)