from django.db import models
from .question import VideoQuestionPair, QuizQuestion

class VideoQuestionChoice(models.Model):
    letter = models.TextField(max_length=10)

    choice_text = models.TextField(max_length=200)

    error_type = models.IntegerField()

    video_question_pair = models.ForeignKey(
        VideoQuestionPair,
        on_delete=models.CASCADE,
        related_name='video_question_choices'
    )


class QuizChoice(models.Model):
    letter = models.TextField(max_length=10)

    choice_text = models.TextField(max_length=200)

    quiz_question = models.ForeignKey(
        QuizQuestion,
        on_delete=models.CASCADE,
        related_name='quiz_choices'
    )