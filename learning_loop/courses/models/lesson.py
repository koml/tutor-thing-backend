from django.db import models
from .section import Section

class Lesson(models.Model):
    title = models.TextField(max_length=100)

    short_title = models.TextField(max_length=20)

    code = models.IntegerField()

    intro_url = models.URLField()

    section = models.ForeignKey(
        Section,
        on_delete=models.CASCADE,
        related_name='lessons'
    )

    def __str__(self):
        return '{} ({})'.format(self.title, self.short_title)