from django.db import models
from .sublesson import Sublesson
from .lesson import Lesson

class VideoQuestionPair(models.Model):
    video_url = models.URLField()

    question_text = models.TextField(max_length=1000)

    correct_choice = models.TextField(max_length=10)

    error_type = models.IntegerField()

    sublesson = models.ForeignKey(
        Sublesson,
        on_delete=models.CASCADE,
        related_name='video_question_pairs'
    )

class QuizQuestion(models.Model):
    question_text = models.TextField(max_length=1000)

    correct_choice = models.TextField(max_length=10)

    explanation_url = models.URLField()

    question_number = models.IntegerField()

    lesson = models.ForeignKey(
        Lesson,
        on_delete=models.CASCADE,
        related_name='quiz_questions'
    )
