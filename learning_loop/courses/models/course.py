from django.db import models

class Course(models.Model):
    title = models.TextField(max_length=100)

    short_title = models.TextField(max_length=20)
    
    code = models.IntegerField()
    
    def __str__(self):
        return '{} ({})'.format(self.title, self.short_title)