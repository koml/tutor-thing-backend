from django.db import models
from .lesson import Lesson

class Sublesson(models.Model):
    title = models.TextField(max_length=100)
    
    short_title = models.TextField(max_length=20)

    code = models.IntegerField()

    lesson = models.ForeignKey(
        Lesson,
        on_delete=models.CASCADE,
        related_name='sublessons'
    )

    def __str__(self):
        return '{} ({})'.format(self.title, self.short_title)