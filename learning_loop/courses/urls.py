from django.conf.urls import url
from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from .views import CourseListView, LessonDetailView

app_name = 'learning_loop.courses'

urlpatterns = [
    url(r'^courses/$', CourseListView.as_view()),
    path('courses/name/<str:course_title>/sections/name/<str:section_title>/lessons/name/<str:lesson_title>', LessonDetailView.as_view())

]

urlpatterns = format_suffix_patterns(urlpatterns)