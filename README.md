## Running the Project Locally:
+ Clone the project
+ Install `virtualenv` for Python
+ In the main project directory, run `virtualenv env`
+ Run `source env/bin/activate` to run the virtual environment
+ Run `pip install -r requirements.txt` to download dependencies.
+ For first time setup, run `python3 manage.py makemigrations` and `python3 manage.py migrate` to set up the database.
+ Run `python3 manage.py runserver` to run the backend. Go to `http://localhost:8080/admin` for the admin page to put data in the database.